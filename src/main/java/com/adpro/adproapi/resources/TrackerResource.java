package com.adpro.adproapi.resources;

import com.adpro.adproservice.model.Site;
import com.adpro.adproservice.model.User;
import com.adpro.adproservice.model.Visitor;
import com.adpro.adproservice.services.SiteService;
import com.adpro.adproservice.services.UserService;
import com.adpro.adproservice.services.VisitorService;
import org.glassfish.jersey.internal.util.Base64;
import org.glassfish.jersey.server.Uri;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.*;
import java.net.URI;
import java.sql.Timestamp;
import java.util.*;

@Path("/tracker")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TrackerResource {

    VisitorService visitorService = new VisitorService();

    @GET
    @Path("/{userId}")
    public Response getTrackingUrl(@PathParam("userId") int userId) {
        URI uri = UriBuilder.fromUri("http://localhost")
                .port(7777)
                .path("tracker")
                .build();

        SiteService siteService = new SiteService();
        List<Site> sites = siteService.getAllSites(userId);
        Map<String, String> mapSites = new HashMap<>();
        for(Site s : sites) {
            String trackingUrl = UriBuilder.fromUri(uri)
                    .path(s.getId())
                    .build()
                    .toString();
            mapSites.put(s.getSiteUrl(), trackingUrl);
        }
        return Response.status(Response.Status.FOUND)
                .entity(mapSites)
                .build();
    }

    @POST
    @Path("/{siteId}")
    public Response addVisitor(@Context HttpHeaders headers, @Context UriInfo uriInfo) {
        String ipAddress = "27.34.65.234";
        String siteId = uriInfo.getPathSegments().get(1).toString();

        if (visitorService.getVisitor(siteId, ipAddress) == null) {
            String userAgent = headers.getRequestHeader("user-agent").get(0);
            long time = headers.getDate().getTime();
            Visitor visitor = new Visitor(siteId, ipAddress, userAgent, time);

            if (visitorService.addVisitor(visitor)) {
                return Response.status(Response.Status.CREATED).build();
            }
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
        return Response.status(Response.Status.FOUND).build();
    }

    @PUT
    @Path("/{siteId}")
    public Response updateVisitor(@Context HttpHeaders headers, @Context UriInfo uriInfo) {
        String ipAddress = "27.34.65.234";
        String siteId = uriInfo.getPathSegments().get(1).toString();
        Visitor visitor = (Visitor) visitorService.getVisitor(siteId, ipAddress);
        if (visitor != null) {
            String userAgent = headers.getRequestHeader("user-agent").get(0);
            long time = headers.getDate().getTime();
            visitor.setUserAgent(userAgent);
            visitor.setTime(time);
            if (visitorService.updateVisitor(visitor)) {
                return Response.status(Response.Status.OK).build();
            }
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
        return Response.notModified().build();
    }

//    DELETE FROM site WHERE time < DATE_SUB(NOW(), INTERVAL 7 DAY);

}
