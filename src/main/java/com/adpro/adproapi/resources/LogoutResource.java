package com.adpro.adproapi.resources;

import com.adpro.adproservice.model.AuthToken;
import com.adpro.adproservice.services.SessionService;
import com.adpro.adproservice.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by ak on 10/8/2016.
 */
@Path("logout")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class LogoutResource {
    SessionService sessionService = new SessionService();
    AuthToken authToken;
    ObjectMapper mapper=new ObjectMapper();

    @POST
    public Response logout(@HeaderParam("Authorization") String authHeader) throws IOException {
        String headerAuthToken = sessionService.getHeaderAuthToken(authHeader); // splitting header and getting token object
        authToken = mapper.readValue(headerAuthToken, AuthToken.class); // json object to authtoken object conversion
        System.out.println(authToken);
        if (sessionService.validateToken(authToken)) {//validate token
            if (sessionService.sessionEnd(authToken)) {
                return Response.status(Response.Status.OK).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();

    }
}
