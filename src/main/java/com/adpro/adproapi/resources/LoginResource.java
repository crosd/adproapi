package com.adpro.adproapi.resources;


import com.adpro.adproservice.model.AuthToken;
import com.adpro.adproservice.model.User;
import com.adpro.adproservice.services.SessionService;
import com.adpro.adproservice.services.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LoginResource {

    UserService userService = new UserService();
    SessionService sessionService = new SessionService();
    AuthToken authToken = null;


    @POST
    public Response loginValidate(User user) {
        if (userService.validateUser(user)) {
            authToken = sessionService.sessionStart(user); //session start.. token authToken generated
            if (authToken != null) {
                return Response.status(Response.Status.ACCEPTED).entity(authToken).build();
            }
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
}


