package com.adpro.adproapi.resources;

import com.adpro.adproservice.model.User;
import com.adpro.adproservice.services.UserService;

import javax.ws.rs.*;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;


@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    UserService userService = new UserService();
    @Context
    ResourceContext resourceContext;

    @POST
    public Response addUser(User user, @Context UriInfo uriInfo) {

        if (userService.addUser(user)) {
            System.out.println(user);
            //   User newUser = userService.getUser(user.getEmailId());
            //  String newId = String.valueOf(newUser.getId());
            // URI uri = uriInfo.getAbsolutePathBuilder()
            //           .path(newId);
            LoginResource log = resourceContext.getResource(LoginResource.class);
            return log.loginValidate(user);
            /*return Response.created(uri)
                    .entity(newUser)
                    .build();
                    */
        }
        return Response.status(Response.Status.NOT_MODIFIED).build();
    }

    @GET
    @Path("/{userId}")
    public Response getUser(@PathParam("userId") int userId) {
        User user = userService.getUser(userId);

        if (user != null) {
            return Response.status(Response.Status.FOUND)
                    .entity(user)
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    public Response getUsers() {
        List<User> users = userService.getAllUsers();
        if (!users.isEmpty()) {
            return Response.status(Response.Status.FOUND)
                    .entity(users)
                    .build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();

    }

    @PUT
    @Path("/{userId}")
    public Response updateUser(User user) {
        if (userService.updateUser(user)) {

            return Response.status(Response.Status.OK)
                    .entity(userService.getUser(user.getEmailId()))
                    .build();
        }
        return Response.status(Response.Status.NOT_MODIFIED).build();
    }

    @DELETE
    @Path("/{userId}")
    public Response deleteUser(@PathParam("userId") int userId) {

        if (userService.removeUser(userId)) {
            return Response.status(Response.Status.NO_CONTENT)

                    .build();
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }
}
