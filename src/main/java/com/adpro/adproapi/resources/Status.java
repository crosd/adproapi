package com.adpro.adproapi.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("status")
//@Api(value="Status", description="Provides api status")
public class Status {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    //@ApiOperation(value="Get status of API server", produces = "text/plain")
    public String getStatus(){
        return "OK";
    }

}
