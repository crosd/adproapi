package com.adpro.adproapi.server;

import com.adpro.adproapi.security.CORSFilter;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;


import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.UUID;


public class AdproApiServer {
    public static void main(String[] args) {
// tis is comment
// change 2
        URI uri = UriBuilder.fromUri("http://localhost").port(7777).build();
        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.packages("com.adpro.adproapi.resources");
        resourceConfig.register(CORSFilter.class);
        Server server = JettyHttpContainerFactory.createServer(uri, resourceConfig);
        try {
            server.start();
            server.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            server.destroy();
        }
    }
}
